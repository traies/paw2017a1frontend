# paw2017a1frontend

# Instalacion del proyecto
- [ ] Instalar Node
- [ ] Instalar Ruby
- [ ] `npm install -g bower grunt-cli yo`
- [ ] `gem install compass`
- [ ] `npm install -g generator-angular-require-fullstack`
- [ ] En el directorio del proyecto `npm install` 
- [ ] En el directorio del proyecto `bower install`
- [ ] Para correr `grunt serve --force`
