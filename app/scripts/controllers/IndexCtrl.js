'use strict';
define(['paw2017a1frontend'], function(paw2017a1frontend) {

	paw2017a1frontend.controller('IndexCtrl', function($scope) {
		$scope.welcomeText = 'Welcome to your paw2017a1frontend page';
	});
});
