'use strict';
define(['paw2017a1frontend'], function(paw2017a1frontend) {

	paw2017a1frontend.controller('HomeCtrl', function($scope) {
		$scope.homePageText = 'This is your homepage';
	});
});
